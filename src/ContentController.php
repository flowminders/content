<?php

namespace FlowControl\Content;

use FlowControl\Content\ContentListView;
use FlowControl\Controllers\AdminController;
use FlowControl\ListView\Columns\Action;
use FlowControl\ListView\ListView;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Kris\LaravelFormBuilder\Facades\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ContentController extends AdminController
{

    use FormBuilderTrait;
    protected $form = ContentForm::class;

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = new ContentListView(
            Content::paginate(20)
        );

        $title = 'Съдържание';
        $this->title([$title, '']);

        return view('flowcontrol::list', compact('list', 'title'));
    }

    /**
     * @param PacketForm $form
     * @return \Illuminate\Http\Response
     */
    public function create(FormBuilder $form)
    {
        $form = $this->form(ContentForm::class, [
            'method' => 'POST',
            'url' => route('admin.content.store'),
        ]);


        $title = 'Създаване на текстово съдържание';
        $this->title([$title, '']);

        return view('flowcontrol::form', compact('form', 'title'));
    }

    /**
     * @param PacketForm $form
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $form = $this->form(ContentForm::class, [
            'method' => 'POST',
        ]);

        $form->redirectIfNotValid();
        $data = collect($form->getFieldValues())->toArray();
        $model = Content::create($data);
        foreach (LaravelLocalization::getLocalesOrder() as $localeCode => $language)
//            dd($localeCode);
            if (LaravelLocalization::getCurrentLocale() == $localeCode) {
                $model->body = request()->get('body_'.$localeCode);
            } else {
//        dd($model);
                $model->translateOrNew($localeCode)->body = request()->get('body_'.$localeCode);
            }
        $model->save();
        $request = $form->getRequest();

        if ($model) {
//            dd($model);
            flash()->success("Успешно създадохте пакет с име \"{$model->key}\"!");
        } else {
            flash()->error("Възникна грешка при създаването на пакет с име \"{$model->key}\". Моля опитайте отново.");
        }

        return redirect(route('admin.content.index'));
    }

    /**
     * @param  int $id
     * @param PacketForm $form
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Content::all()->find($id);

        $form = $this->form(ContentForm::class, [
            'method' => 'PUT',
            'url' => route('admin.content.update', $id),
            'model' => $model,
        ]);


        $title = 'Редактиране на пакет';
        $this->title([$title, '']);

        return view('flowcontrol::form', compact('form', 'title'));
    }

    /**
     * @param  int $id
     * @param PacketForm $form
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $model = Content::findOrFail($id);
        $form = $this->form(ContentForm::class, ['model' => $model]);
        $form->redirectIfNotValid();

        $data = collect($form->getFieldValues())->toArray();
        foreach (LaravelLocalization::getLocalesOrder() as $localeCode => $language)
//            dd($localeCode);
            if (LaravelLocalization::getCurrentLocale() == $localeCode) {
                $model->body = request()->get('body_'.$localeCode);
            } else {
//        dd($model);
                $model->translateOrNew($localeCode)->body = request()->get('body_'.$localeCode);
            }

        try {
            $model->update($data);
            $request = $form->getRequest();
            flash()->success("Успешно редактирахте пакет с име \"{$model->key}\"!");
        } catch (Exception $e) {
            throw $e;
            flash()->error("Възникна грешка при редактирането на пакет с име \"{$model->key}\". Моля опитайте отново.");
        }

        return back();
    }

}
