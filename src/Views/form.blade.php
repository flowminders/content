@extends('flowcontrol::layout.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div id="l-switcher-btns">
                @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $language)
                    @if(LaravelLocalization::getCurrentLocale() == $localeCode)
                        <button class="btn btn-primary active"
                                data-switch="{{$localeCode}}">{{$language['name']}}</button>
                    @else
                        <button class="btn btn-default" data-switch="{{$localeCode}}">{{$language['name']}}</button>
                    @endif
                @endforeach
            </div>
            <br/>
            <div class="box">
                <div class="box-header">
                    <a type="button" href="{{route('admin.content.index') }}" style="float: right" class="btn"><i
                                class="fa fa-arrow-circle-left"></i> &nbsp; Go back</a>

                    <h3 class="box-title">{{ $title or '' }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! form($form) !!}

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@stop