var Content = {
    formSelector: '#ContentForm',
    formContainerSelector: 'component-editor-container',
    editBtnSelector: 'component-content',
    editTextSelector: 'component-content-text',
    startTextValue: null,
    closeBtn: 'component-editor-container component-editor .close',
    editId: 0,
    editText: null,
    setEditText: function (id) {
        this.editText.html(tinyMCE.activeEditor.getContent());
    },
    formClose: function () {
        $(this.formContainerSelector).hide(200);
    },
    formOpen: function () {
        $(this.formContainerSelector).show(200);
    },
    successSave: function () {
        var submit = $(this.formSelector + ' input[type=submit]');
        submit.prop('disabled', false);
        submit.removeClass('disabled');
        submit.addClass('success');
        setTimeout(function () {
            submit.removeClass('success');
        }, 1000);
    },
    loadSave: function () {
        $(this.formSelector + ' input[type=submit]').prop('disabled', true);
        $(this.formSelector + ' input[type=submit]').addClass('disabled');
    },
    noSaveAlert: function () {
        if (this.isSaveAlert()) {
            return true;
        }
        return confirm("Не сте запазили. Искате ли да затворите формата.");
    },
    isSaveAlert: function () {
        if (this.editText.html().trim() == this.startTextValue.trim()) {
            return true;
        }
        return false;
    },
    setFormContent: function (id) {
        this.editId = id;
        this.editText = $(this.editTextSelector + "[data-id='" + this.editId + "']");
        tinyMCE.activeEditor.setContent(this.editText.html());
        $(this.formSelector + ' input[name=id]').val(this.editId);
        this.startTextValue = this.editText.html();
        this.formOpen();
    },
    save: function () {
        var token = $(this.formSelector + ' input[name="_token"]').val();
        var data = {name: tinyMCE.activeEditor.getContent(), key: this.editKey};
        this.startTextValue = this.editText.html();
        var Object = this;
        $.ajax({
            type: "PUT",
            url: "admin/content/" + this.editId,
            data: data
        }).done(function (data) {
            Object.successSave();
        });
    },
    init: function () {
        var Object = this;
        $(Object.editBtnSelector).click(function () {
            Object.setFormContent($(this).data('id'));
        });
        $(Object.closeBtn).click(function () {
            if (Object.noSaveAlert()) {
                Object.formClose();
            }
        });
        $(Object.editBtnSelector).click(function () {
            Object.editId = $(this).data('id');
            Object.editText = $(Object.editTextSelector + "[data-id='" + Object.editId + "']");
            Object.editKey = $(Object.editBtnSelector + "[data-id='" + Object.editId + "']").data('key');
            tinyMCE.activeEditor.setContent(Object.editText.html());
            Object.formOpen();
        });
        $(Object.formSelector).submit(function (e) {
            e.preventDefault();
            Object.loadSave();
            Object.save();
        });


        tinymce.init({
            selector: 'component-editor-container textarea',
            skin: "light",
            setup: function (editor) {
                editor.on('change keyup', function (e) {
                    $(Object.setEditText(Object.editId));
                });
            },
            force_br_newlines: false,
            force_p_newlines: false,
            forced_root_block: ''
        });
        window.onbeforeunload = function (e) {
            if (!Object.isSaveAlert()) {
                return "Не сте запазили. Искате ли да затворите формата.";
            }
        };
    },
};
Content.init();