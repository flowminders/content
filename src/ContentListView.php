<?php

namespace FlowControl\Content;

use FlowControl\Filters\Filters;
use FlowControl\ListView\ListView;
use FlowControl\ListView\Columns\Action;
use FlowControl\ListView\Columns\Actions;
use FlowControl\ListView\Columns\Column;

class ContentListView extends ListView
{
    public function __construct($dataSource = null)
    {
        parent::__construct($dataSource);

        $this->class = 'table table-bordered table-hover table-striped';
    }

    protected function columns()
    {

        $this
            ->text('id', '#ID', ['sortable' => false])
            ->text('key', 'Ключ на съдържанието', ['sortable' => false])
            ->wysiwyg('body', 'Стойност')
            ->actions('', function (Actions $actions) {
//                $actions
//                    ->action('add', 'Добави')
//                    ->icon('fa fa-plus')
//                    ->url(route('admin.content.create'))
//                    ->setGlobal();

                $actions
                    ->action('edit', '')
                    ->icon('fa fa-edit')
                    ->define(function(Action $action, array $row){
                        $action->url(route('admin.content.edit', [$row['id']]));
                    });
            })
        ;
    }

    protected function filters(Filters $filter)
    {
        $filter
            ->text('key', 'Ключ')
            ->text('name', 'Стойност')
        ;
    }


}

