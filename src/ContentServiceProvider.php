<?php

namespace FlowControl\Content;

use Illuminate\Support\ServiceProvider;

class ContentServiceProvider extends ServiceProvider
{

    public function boot()
    {
        include __DIR__ . '/helpers.php';

        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->loadViewsFrom(__DIR__ . '/Views', 'flowcontrol');

        $this->publishes([
            __DIR__ . '/Database/migrations' => database_path('migrations')
                ], 'migrations');

        $this->publishes([
            __DIR__ . '/Database/seeds' => database_path('seeds')
                ], 'seeds');

        $this->publishes([
            __DIR__ . '/Config/content.php' => config_path('flowcontrol.content.php')
                ], 'config');

        $this->publishes([
            __DIR__ . '/Public/onpage-edit' => public_path('vendor/onpage-edit'),
                ], 'public');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
//        $app = $this->app;
//
//        $this->app->bind(Localizator::class, function() use ($app) {
//            return new Localizator($app['app'], $app['session.store'], $app['url']);
//        });
//
//        $this->app->alias(Localizator::class, 'flowcontrol.localizator');
//
//        $this->mergeConfigFrom(__DIR__ . '/Config/flowcontrol.content.php', 'content');
    }

}
