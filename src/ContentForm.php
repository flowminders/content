<?php

namespace FlowControl\Content;

use FlowControl\Content\BaseForm;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ContentForm extends BaseForm
{

    protected $clientValidationEnabled = false;

    /**
     * Define the fields of the form.
     *
     * @param FormBuilder $form
     */
    public function buildForm()
    {
        $model = $this->getModel();
        $this->add('key', 'text', [
            'label' => 'Ключ',
            'rules' => 'required'
        ]);
            foreach (LaravelLocalization::getLocalesOrder() as $localeCode => $language) {
                if ($model != null) {
//            dump($model->translate($localeCode)->body);
//                    dump($model->translate($localeCode));
                    if ($model->translate($localeCode)) {
                        $this->add('body_' . $localeCode, 'textarea', [
                            'wrapper' => ['data-lang' => $localeCode, 'class' => 'multilanguage'],
                            'attr' => ['class' => 'tinymce-textarea'],
                            'value' => $model->translate($localeCode)->body,
                            'label' => 'Текст',
                            'rules' => 'required'
                        ]);
                    } else {
                        $this->add('body_' . $localeCode, 'textarea', [
                            'wrapper' => ['data-lang' => $localeCode, 'class' => 'multilanguage'],
                            'attr' => ['class' => 'tinymce-textarea'],
                            'label' => 'Текст',
                            'rules' => 'required'
                        ]);
                    }
                } else {
                    $this->add('body_' . $localeCode, 'textarea', [
                        'wrapper' => ['data-lang' => $localeCode, 'class' => 'multilanguage'],
                        'attr' => ['class' => 'tinymce-textarea'],
                        'label' => 'Текст',
                        'rules' => 'required'
                    ]);
                }
            }
        $this->add('submit', 'submit', [
            'label' => 'Запази',
            'attr' => ['class' => 'btn btn-primary'],
        ]);
//            ->wysiwyg('name', 'Текст', ['translated' => true])
//            ->submit('save', 'Запази');
    }

}
