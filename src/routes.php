<?php

use Illuminate\Http\Request;

Route::group(['middleware' => ['flowcontrol.admin', 'flowcontrol.auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::resource('content', '\FlowControl\Content\ContentController');
});
