<?php

namespace FlowControl\Content;

use Illuminate\Database\Eloquent\Model;

class ContentTranslation extends Model {

    public $timestamps = false;
    protected $table = 'flowcontrol_content_translations';
    protected $fillable = ['body'];

}
