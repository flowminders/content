<?php

namespace FlowControl\Content;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Content extends Model
{

    use Translatable;
    protected $table = 'flowcontrol_content';
    public $translatedAttributes = ['body'];
    protected $fillable = ['body','key'];

    public static function getContent($key)
    {
        return Content::translated()->where('key', '=', $key)->firstOrFail();
    }

    public static function existsContent($key)
    {

        $return = Content::translated()->where('key', $key)->first();

        if ($return === null) {
            return false;
        }
        return true;
    }

}
