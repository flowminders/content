<?php

namespace FlowControl\Content;


use Illuminate\Support\Str;
use Kris\LaravelFormBuilder\Form;

class BaseForm extends Form
{

    private $createRoutes = ['create', 'store'];

    private $editRoutes = ['edit', 'update', 'change'];

    public function isCreate()
    {
        return Str::contains($this->getRequest()->route()->getActionName(), $this->createRoutes);
//        return Str::endsWith($this->getRequest()->route()->getActionName(), $this->createRoutes);
    }

    public function isEdit()
    {
        return Str::contains($this->getRequest()->route()->getActionName(), $this->editRoutes);
    }

}