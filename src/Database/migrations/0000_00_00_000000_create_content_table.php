<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('flowcontrol_content', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 200)->nullable();
            $table->timestamps();
        });
        Schema::create('flowcontrol_content_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->text('body');
            $table->string('locale')->index();

            $table->unique(['content_id','locale']);
            $table->foreign('content_id')->references('id')->on('flowcontrol_content')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('flowcontrol_content_translations');
        Schema::drop('flowcontrol_content');
    }

}
