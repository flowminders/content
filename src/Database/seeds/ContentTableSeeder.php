<?php

use FlowControl\Content\Content;
use FlowControl\Localization\Database\TranslationSeeder;

class ContentTableSeeder extends TranslationSeeder {

    protected $model = Content::class;

    protected function get() {
        return collect([
            [
                'info' => ['key' => 'meta.description.contacts'],
                'translations' => [
                    1 => ['name' => ''], //en
                    2 => ['name' => 'meta description'], //bg
                ],
            ],
        ]);
    }

}
