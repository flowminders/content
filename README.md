# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This packet adds Content module in the Admin module and also offer the on page edit functionality

### How do I get set up? ###

* Add this to the composer.json

```
#!json

  "require": {
    "flowcontrol/content":"dev-master"
  },


    "repositories":[
        {
            "type": "vcs",
            "url": "git@bitbucket.org:flowminders/content.git"
        }
    ],
```

You should be authenticated to BitBucket cause this is private repo

Than do: 
- composer update
- vendor publish

### Contribution guidelines ###

* This packet requires flowcontrol/admin 

### Who do I talk to? ###

* Misho, Teo, Petar in that order